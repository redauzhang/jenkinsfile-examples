pipeline {
  agent any
  stages {
    stage("检出") {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: [[name: GIT_BUILD_REF]],
          userRemoteConfigs: [[
            url: GIT_REPO_URL,
            credentialsId: CREDENTIALS_ID
        ]]])
      }
    }

    stage('执行测试代码') {
      steps {
        echo "running test"
      }
    }

    stage('部署到开发集成环境') {
      when {
        branch "dev"
      }
      steps {
        echo "running deliver-for-dev.sh"
        // sh "./jenkins/scripts/deliver-for-dev.sh"
      }
    }

    stage('部署到生产环境') {
      when {
        branch "master"
      }
      steps {
        echo "running deploy-for-production.sh"
        // sh "./jenkins/scripts/deploy-for-production.sh"
      }
    }

  }
}